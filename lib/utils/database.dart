import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DBProvider{
  DBProvider._();
  Database _database;
  static final DBProvider db = DBProvider._();

  Future<Database> get database async {
    if(_database == null) {
      _database = await initDB();
    }
    return _database;

  }
  initDB() async {
    return await openDatabase(
      join(await getDatabasesPath(), 'cpasskey.db'),
      onCreate: (db, version) async {
        await db.execute('''
          CREATE TABLE password(
            number INTEGER PRIMARY KEY,
            username TEXT,
            email TEXT,
            password TEXT
          )
        ''');
      }
    );
  }

  insertPass(insertPass) async {
    final db = await database;
    var res = await db.rawInsert('''
      INSERT INTO password(
        number, username, email, password
      ) VALUES (?,?)
    ''', [insertPass.number, insertPass.username, insertPass.email, insertPass.password]);

    return res;
  }


}


