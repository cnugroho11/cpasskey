import 'package:flutter/material.dart';

double sizeHorizontal;
double sizeVertical;
double pix;

class SizeConfig {
  static MediaQueryData  _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal = _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
    sizeHorizontal = safeBlockHorizontal;
    sizeVertical = safeBlockVertical;
    print('hor : '+sizeHorizontal.toString());
    print('ver : '+sizeVertical.toString());
    pix = (sizeVertical + sizeHorizontal)/2;

  }
}

//--------Dracula Color Section----------
const Color youngBlue = Color(0xFFB5E1F6);
const Color mainBackground = Color(0xff282a36);
const Color altBackground = Color(0xff44475a);
const Color foreground = Color(0xfff8f8f2);
const Color cmtColor = Color(0xff6272a4);
const Color cyan = Color(0xff8be9fd);
const Color green = Color(0xff50fa7b);
const Color orange = Color(0xffffb86c);
const Color pink = Color(0xffff79c6);
const Color purple = Color(0xffff79c6);
const Color red = Color(0xffff5555);
const Color yellow = Color(0xfff1fa8c);
//-------------------------------------