import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cpass_key/constants/theme.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isInit = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    forInit();
  }

  forInit() {
    Timer(Duration(milliseconds: 3000), () async {
      Navigator.of(context).popAndPushNamed('Home');
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isInit) {
      SizeConfig().init(context);
      isInit = false;
    }

    return Scaffold(
      backgroundColor: mainBackground,
      body: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: SizeConfig.screenHeight / 2.5),
            Icon(
              Icons.lock_rounded,
              size: 100,
              color: Colors.white,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'CPass',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                ),
                Text(
                  'Key',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                )
              ],
            ),
            SizedBox(
              height: SizeConfig.screenHeight / 3,
            ),
            Text(
              'everything is dracula here',
              style: TextStyle(
                  color: Colors.white, fontSize: 11),
            ),
            Text(
              'by Cahyo Nugroho',
              style: TextStyle(color: Colors.white, fontSize: 12),
            )
          ],
        ),
      ),
    );
  }
}
