import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cpass_key/constants/theme.dart';
import 'package:cpass_key/widgets/title_text.dart';
import 'package:cpass_key/widgets/drawer_widget.dart';
import 'package:cpass_key/widgets/password_card.dart';
import 'package:cpass_key/pages/create_password_screen.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerWidget(),
      backgroundColor: mainBackground,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: TitleText(title1: 'CPass', title2: 'Key',)
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              PasswordCard(),
            ],
          )
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => CreatePasswordScreen()));
        },
        backgroundColor: red,
        child: Icon(Icons.add),
      ),
    );
  }
}
