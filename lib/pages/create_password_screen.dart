import 'package:flutter/material.dart';
import 'package:cpass_key/constants/theme.dart';
import 'package:cpass_key/widgets/title_text.dart';

import '../constants/theme.dart';
import '../constants/theme.dart';


class CreatePasswordScreen extends StatefulWidget {
  @override
  _CreatePasswordScreenState createState() => _CreatePasswordScreenState();
}

class _CreatePasswordScreenState extends State<CreatePasswordScreen> {
  final _siteName = TextEditingController();
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: TitleText(
          title1: 'Enter your ',
          title2: 'credential',
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              width: 300,
              child: Text('Site',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 23,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
                width: 350,
                height: 45,
                decoration: BoxDecoration(
                    color: altBackground,
                    borderRadius: BorderRadius.all(Radius.circular(30)
                    )
                ),
                child: TextField(
                  controller: _siteName,
                  style: TextStyle(
                    color: Colors.white
                  ),
                  decoration: new InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(30.0),
                      ),
                    ),
                    //filled: true,
                    hintStyle: new TextStyle(color: Colors.white38),
                    hintText: "Type in your text",
                    //fillColor: Colors.white70
                  ),
                )
            ),
            SizedBox(height: 10,),
            Container(
              width: 300,
              child: Text('Username',
                style: TextStyle(
                    color: pink,
                    fontSize: 23,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
                width: 350,
                decoration: BoxDecoration(
                    color: altBackground,
                    borderRadius: BorderRadius.all(Radius.circular(30)
                    )
                ),
                child: TextField(
                  controller: _usernameController,
                  style: TextStyle(
                      color: Colors.white
                  ),
                  decoration: new InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(30.0),
                      ),
                    ),
                    //filled: true,
                    hintStyle: new TextStyle(color: Colors.white38),
                    hintText: "Type in your text",
                    //fillColor: Colors.white70
                  ),
                )
            ),
            SizedBox(height: 10,),
            Container(
              width: 300,
              child: Text('Email',
                style: TextStyle(
                    color: cyan,
                    fontSize: 23,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
                width: 350,
                decoration: BoxDecoration(
                    color: altBackground,
                    borderRadius: BorderRadius.all(Radius.circular(30)
                    )
                ),
                child: TextField(
                  controller: _emailController,
                  style: TextStyle(
                      color: Colors.white
                  ),
                  decoration: new InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(30.0),
                      ),
                    ),
                    //filled: true,
                    hintStyle: new TextStyle(color: Colors.white38),
                    hintText: "Type in your text",
                    //fillColor: Colors.white70
                  ),
                )
            ),
            SizedBox(height: 10,),
            Container(
              width: 300,
              child: Text('Password',
                style: TextStyle(
                    color: green,
                    fontSize: 23,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            SizedBox(height: 10,),
            Container(
                width: 350,
                decoration: BoxDecoration(
                    color: altBackground,
                    borderRadius: BorderRadius.all(Radius.circular(30)
                    )
                ),
                child: TextField(
                  controller: _passwordController,
                  style: TextStyle(
                      color: Colors.white
                  ),
                  decoration: new InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(30.0),
                      ),
                    ),
                    //filled: true,
                    hintStyle: new TextStyle(color: Colors.white38),
                    hintText: "Type in your text",
                    //fillColor: Colors.white70
                  ),
                )
            ),
            SizedBox(height: 10,)
          ],
        ),
      ),
    );
  }
}
