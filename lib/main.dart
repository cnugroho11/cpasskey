import 'package:cpass_key/constants/theme.dart';
import 'package:flutter/material.dart';
import 'package:cpass_key/pages/splash_screen.dart';
import 'package:cpass_key/pages/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        canvasColor: mainBackground,
      ),
      home: SplashScreen(),
      routes: {
        'Root' : (BuildContext context) => SplashScreen(),
        'Home' : (BuildContext context) => HomePage(),
      },
    );
  }
}
