import 'package:flutter/material.dart';
import 'package:cpass_key/constants/theme.dart';

import '../constants/theme.dart';

class PasswordCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 20,),
          Container(
            width: SizeConfig.screenWidth / 1.3,
            height: SizeConfig.screenHeight / 5.5,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: altBackground
            ),
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Text('Facebook',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(height: SizeConfig.screenHeight / 70,),
                  Container(
                    width: 250,
                    child: Text('usernameTest',
                      style: TextStyle(
                          color: pink,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Container(
                    width: 250,
                    child: Text('email@test.com',
                      style: TextStyle(
                          color: cyan,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  Container(
                    width: 250,
                    child: Text('tap to see password',
                      style: TextStyle(
                          color: green
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
