import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {

  String title1;
  String title2;

  TitleText({this.title1, this.title2});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title1,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
        Text(
          title2,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.red),
        ),
      ],
    );
  }
}
