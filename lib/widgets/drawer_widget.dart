import 'package:flutter/material.dart';
import 'package:cpass_key/constants/theme.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Text('Coba header'),
            decoration: BoxDecoration(
                color: Colors.blue
            ),
          )
        ],
      ),
    );
  }
}
